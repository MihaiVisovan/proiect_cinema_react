﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaVision.Models
{
    public class MyContext : DbContext
    {
        public MyContext(DbContextOptions<MyContext> options):base(options)
        {
         
        }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieHour> MovieHours { get; set; }
        public DbSet<Chair> Chairs { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
