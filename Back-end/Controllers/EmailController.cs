﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using CinemaVision.Models;
using CinemaVision.Services;
using Microsoft.AspNetCore.Mvc;

namespace CinemaVision.Controllers
{
    public class EmailController : Controller
    {
        private readonly MyContext db;

        public EmailController(MyContext db)
        {
            this.db = db;
        }

        [HttpPost]
        [Route("api/Email/Send")]
        public int Send([FromBody]string[] stringToSend)
        {
            SendEmail sendEmail = new SendEmail(stringToSend);
            return 1;
        }
    }
}