﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaVision.BusinessLayer;
using CinemaVision.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CinemaVision.Controllers
{
    public class MovieController : Controller
    {

        private readonly MyContext db;

        public MovieController(MyContext db)
        {
            this.db = db;
        }

        [HttpGet]
        [Route("api/Movie/Index")]
        public IEnumerable<Movie> Index()
        {
            try
            {
                IList<Movie> movies =  db.Movies.Include(hour => hour.Hours).ToList();
                Console.Write(movies);
                return movies;
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        [Route("api/Movie/Create")]
        public int Create(Movie movie)
        {
            db.Movies.Add(movie);
            db.SaveChanges();
            return 1;
        }

        [HttpGet]
        [Route("api/Movie/Details/{id}")]
        public Movie Details(int id)
        {
            Movie movie = db.Movies.Include(hour => hour.Hours).Where(m => m.MovieId == id).FirstOrDefault();
            return movie;
        }

        [HttpPut]
        [Route("api/Movie/Edit")]
        public int Edit(Movie movie)
        {
            db.Entry(movie).State = EntityState.Modified;
            db.SaveChanges();
            return 1;
        }

        [HttpDelete]
        [Route("api/Movie/Delete/{id}")]
        public int Delete(int id)
        {
            Movie movie = db.Movies.Find(id);
            db.Movies.Remove(movie);
            db.SaveChanges();
            return 1;
        }
    }
}
