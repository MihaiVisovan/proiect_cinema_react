﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaVision.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CinemaVision.Controllers
{
    public class MovieHourController : Controller
    {

        private readonly MyContext db;

        public MovieHourController(MyContext db)
        {
            this.db = db;
        }

        [HttpGet]
        [Route("api/MovieHour/Chairs/{id}")]
        public MovieHour Details(int id)
        {
            MovieHour movieHour = db.MovieHours.Include(chair => chair.Chairs).Where(m => m.MovieHourId == id).FirstOrDefault();
            return movieHour;
        }

        [HttpPut]
        [Route("api/MovieHour/Edit")]
        public int Edit([FromBody]MovieHour movieHour)
        {
            db.Update(movieHour);
            db.SaveChanges();
            return 1;
        }
    }
}