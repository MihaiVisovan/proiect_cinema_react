﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaVision.Models;
using Microsoft.AspNetCore.Mvc;

namespace CinemaVision.Controllers
{
    public class UserController : Controller
    {

        private readonly MyContext db;

        public UserController(MyContext db)
        {
            this.db = db;
        }

        [HttpPost]
        [Route("api/User")]
        public bool Details(string Email, string Password)
        {
            User user = db.Users.Where(u => u.Email == Email && u.Password  == Password).FirstOrDefault();
            if(user != null)
            return true;

            return false;
        }
    }
}