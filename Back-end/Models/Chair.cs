﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaVision.Models
{
    public class Chair
    {
        [Key]
        public int ChairId { get; set; }
        public int MovieHourId { get; set; }
        public bool Occupied { get; set; }
    }
}
