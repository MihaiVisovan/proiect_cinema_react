﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace CinemaVision.Models
{
    public class Movie
    {
        [Key]
        public int MovieId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Rating { get; set; }
        public string Category { get; set; }
        public string Type { get; set; }
        public string Distribution { get; set; }
        public string Director { get; set; }
        public string Language { get; set; }
        public int Time { get; set; }
        public DateTime LaunchDate { get; set; }
        public IList<MovieHour> Hours { get; set; }
        public byte[] Image { get; set; }
    }
}
