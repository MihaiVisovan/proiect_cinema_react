﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaVision.Models
{
    public class MovieHour
    {
        [Key]
        public int MovieHourId { get; set; }
        public string StringHour { get;set;}
        public int MovieId { get; set; }
        public IList<Chair> Chairs { get; set; }
    }
}
