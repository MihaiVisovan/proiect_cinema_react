import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { MovieData, HourData} from './FetchMovie';
import { Link } from 'react-router-dom';

interface FetchMovieDataState {
    movieList: MovieData[];
    loading: boolean
}

export class Admin extends React.Component<RouteComponentProps<{}>, FetchMovieDataState> {
    constructor() {
        super();
        this.state = { movieList: [], loading: true };
        fetch('api/Movie/Index')
            .then(response => response.json() as Promise<MovieData[]>)
            .then(data => {
                this.setState({ movieList: data, loading: false });
            });
    }

    public render() {
        let contents = this.state.loading
        ? <p><em>Loading...</em></p>
        : this.renderMovieTable(this.state.movieList);

        return <div>
                    <div className = 'motto-style'>
                Our <span className = 'color-style'> movies </span> depend on your <span className = 'color-style'> Vision! </span>
                    </div>
                    <p>
                      <Link to="updateMovie/0">Create New</Link>
                   </p>
                    {contents}
               </div>
    }


    private renderMovieTable(movieList: MovieData[]) {
        return <table className='table'>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Rating</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Distribution</th>
                    <th>Director</th>
                    <th>Language</th>
                    <th>Time</th>
                    <th>LaunchDate</th>
                    <th>Edit Movie</th>
                    <th>Delete Movie</th>
                </tr>
            </thead>
            <tbody>
            {movieList.map(movie =>
            
                    <tr key={movie.movieId}>
                        <td>{movie.name}</td>
                        <td>{movie.description}</td>
                        <td>{movie.rating}</td>
                        <td>{movie.category}</td>
                        <td>{movie.type}</td>
                        <td>{movie.distribution}</td>
                        <td>{movie.director}</td>
                        <td>{movie.language}</td>
                        <td>{movie.time}</td>
                        <td>{movie.launchDate}</td>
                        <td>
                            <a className="action" onClick={(movieId) => this.handleEdit(movie.movieId)}>Edit</a>
                        </td>
                        <td>
                            <a className="action" onClick={(movieId) => this.handleDelete(movie.movieId)}>Delete</a>
                        </td>
                    </tr>
                )}
            </tbody>
        </table>;
    }

    private handleDelete(movieId: number) {
        if (!confirm("Do you want to delete movie with Id: " + movieId))
            return;
        else {
            fetch('api/Movie/Delete/' + movieId, {
                method: 'delete'
            }).then(data => {
                this.setState(
                    {
                        movieList: this.state.movieList.filter((rec) => {
                            return (rec.movieId != movieId);
                        })
                    });
            });
        }

    }

    private handleEdit(movieId: number) {
        console.log(movieId)
        this.props.history.push("/updateMovie/" + movieId);
    }
}