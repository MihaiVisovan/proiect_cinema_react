import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import './../css/chairs.css';
import * as jspPDF from 'jspdf';

interface ChairSelectDataState {
    hour: HourData;
    loading: boolean,
    variabila : boolean,
    email : string,
    name: string,
    stringToSend: string[],
    chairList: string
}

export class ChairSelect extends React.Component<RouteComponentProps<any>, ChairSelectDataState> {
    constructor() {
        super();    
        this.state = {loading: true, hour: new HourData, variabila: true, email :"", name : "", stringToSend : [""], chairList : ""};
    }

    public render()
    { 
        let contents = this.state.loading
        ? <p><em>Loading...</em></p>
        : this.printChairs(this.state.hour.chairs);

        const idHour = this.props.match.params.idHour;
        if (idHour > 0 && this.state.variabila == true)
        {
            fetch('api/MovieHour/Chairs/' + idHour)
                .then(response => response.json() as Promise<HourData>)
                .then(data => {
                    this.setState({ loading: false, hour: data });
                });
                this.setState({variabila : false});
        }
         return <div>
         <div className = 'motto-style'>
          Our <span className = 'color-style'> movies </span> depend on your <span className = 'color-style'> Vision! </span>
         </div>
         <img src={require('./../images/CinemaTv.png')}width="70%" height="100%" className= "cinema-tv-style"/>
                    {contents}
        </div>
    }
    

    private makeDoc(stringPdf : string[])
    {
            var toPrint = "The client " + stringPdf[1] + " has reserved the following chairs \n" + this.state.chairList;
            var doc = new jspPDF(); 
            doc.text(toPrint,10,10);
            doc.save("Reservation.pdf");
    }
    

    private sendEmail()
    {
        const stringToSend = this.state.stringToSend;
        stringToSend[0] = this.state.email;
        stringToSend[1] = this.state.name;
        stringToSend[2] = this.state.chairList;
        const data = JSON.stringify(stringToSend);
        
        fetch('api/Email/Send', {
            method: 'POST',
            body: data,
            headers: {
                'content-type': 'application/json',
            }
        }).then((response) => response.json())
            .then((responseJson) => {
                this.props.history.push("/movies");
                alert("Your reservation is complete .Check your email for more details")
            })

        this.makeDoc(stringToSend);

    }
    private printEmailTable()
    {
        return (
            <div className = "mail-padding">
                <form className="mail">
                    <img src={require('./../images/navbarLogo.png')} className = "mail-cinema" />
                    <input 
                        type="text"
                        className="mail-input" 
                        required
                        onChange={event => this.setState({email: event.target.value})}
                        placeholder ="Email address..."
                    />
                    <input
                    type="text" 
                    className="mail-input"
                    required
                    onChange={event => this.setState({name: event.target.value})}
                    placeholder ="Name..."
                    />
                    <button  className="mail-button" onClick = {(e) => this.updateChairs(e)}>
                    Make Reservation!
                    </button>
                </form>
                </div>
            )
    }

    private gotToDetails()
    {
        this.props.history.push("/details/" + this.props.match.params.idMovie)
    }

    private updateChairs(e:any)
    {
        e.preventDefault();
        if (!confirm("Do you want to make a reservation with these seats? "))
            return;
        else 
        {
            fetch('api/MovieHour/Edit', {
                method: 'PUT',
                body: JSON.stringify(this.state.hour),
                headers: {
                    'content-type': 'application/json',
                }
                }).then(res=>res.json())
                .then(res =>{});
        }  

        this.sendEmail();
    }
    private changeChairColor(chair: ChairData)
    {
        let newHour = this.state.hour;
        let position = newHour.chairs.indexOf(chair);
        let oldOccupied = newHour.chairs[position].occupied;
        newHour.chairs[position].occupied = !oldOccupied;
        this.setState({hour: newHour});


        var chairListNew = this.state.chairList;
        if(!oldOccupied == true)
        {
            chairListNew += "/" + position;
            this.setState({chairList : chairListNew});
        }
    }


    private printChairs(chairs: ChairData[])
    {
    return <div>
    <div className = "big-chair-style">
                    <div className = "small1-chair-style">
                        {chairs.filter(chair => chair.chairId %100 < 50).map(chair =>
                                            chair.occupied ? (
                                                this.state.variabila ? (
                                                            <div className = "chair-style">
                                                                <button className = "occupied-chair" > </button>
                                                            </div>
                                                                ): (
                                                                    <div className = "chair-style">
                                                                    <button className = "occupied-chair" onClick= {() => this.changeChairColor(chair)}> </button>
                                                                </div>
                                                                )
                                                            ) : (
                                                    <div className = "chair-style">
                                                        <button className = "chair" onClick = {() => this.changeChairColor(chair)}> </button>
                                                    </div>)
                        )}
                    </div>
                    <div className = "small2-chair-style">
                            {chairs.filter(chair => chair.chairId %100 >= 50 ).map(chair =>
                                             chair.occupied ? (
                                                this.state.variabila ? (
                                                    <div className = "chair-style">
                                                        <button className = "occupied-chair" > </button>
                                                    </div>
                                                        ): (
                                                            <div className = "chair-style">
                                                            <button className = "occupied-chair" onClick= {() => this.changeChairColor(chair)}> </button>
                                                        </div>
                                                        )
                                                    ) : (
                                                <div className = "chair-style">
                                                    <button className = "chair" onClick = {() => this.changeChairColor(chair)}> </button>
                                                </div>)
                                )}  
                    </div>
                        <div className="table-style">{this.printEmailTable()}
                    </div>
             </div>

             <button className="back-button-chair" type="submit" onClick = {() => this.gotToDetails()}><i className="glyphicon glyphicon-circle-arrow-left"></i> Back </button>

             </div>
        }
    }

 export class HourData{
    chairs: ChairData [] = [new ChairData()];
    movieHourId: number = 0;
    movieId : number = 0;
    stringHour: string = "";
}

export class ChairData{
    chairId: number = 0;
    movieHourId: number = 0;
    occupied: boolean = false;
}
