import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { MovieData, HourData} from './FetchMovie';



interface DetailsDataState {
    movie: MovieData;
    loading: boolean
}


export class Details extends React.Component<RouteComponentProps<any>, DetailsDataState> {
    constructor() {
        super();
        this.state = {loading: true, movie: new MovieData };
 
    }
    public render()
    {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.printDetails();

        const id = this.props.match.params.id;
        if (id > 0)    
        {
            fetch('api/Movie/Details/' + id)
                .then(response => response.json() as Promise<MovieData>)
                .then(data => {
                    this.setState({ loading: false, movie: data });
                });
        }
         return <div> 
         <div className = 'motto-style'>
          Our <span className = 'color-style'> movies </span> depend on your <span className = 'color-style'> Vision! </span>
         </div>
         {contents}
         </div>
    }


    private goToChairSelect(idHour: number)
    {
        this.props.history.push("/chairSelect/" + this.props.match.params.id +"/hour/" + idHour)

    }
    private goToMovieList()
    {
        this.props.history.push("/movies")
    }

    private printDetails()
    {

        let hours = this.state.movie.hours;
        let dateString = this.state.movie.launchDate.toString();
        let date = new Date(dateString);
  
        const DATE_OPTIONS = { year: 'numeric', month: 'long', day: 'numeric' };
        let finalDate = date.toLocaleDateString('en-US', DATE_OPTIONS);

        return<div className="padding-total-description"> 
            <div className = 'schedule-style'>
                Movie Details
            </div>
            <br>
            </br>
        <div className="display-all-inline">
            <table>
                <th>
                    <div className="title-description-style">
                        <tr>Title: <td> <p className ="description-description-style">  {this.state.movie.name} </p> </td> <td> </td></tr>
                        <tr>Rating: <td><p className ="description-description-style">  {this.state.movie.rating}</p></td> </tr>
                        <tr>Type: <td><p className ="description-description-style">  {this.state.movie.type}</p></td> </tr>
                        <tr>Director: <td> <p className ="description-description-style">  {this.state.movie.director}</p> </td> </tr>
                        <tr>Distribution:<td> <p className ="description-description-style">  {this.state.movie.distribution}</p> </td></tr>
                        <tr>Language: <td><p className ="description-description-style">  {this.state.movie.language}</p> </td> </tr>
                        <tr>Duration: <td><p className ="description-description-style">  {this.state.movie.time}</p></td> </tr>
                        <tr>Launch Date:
                            <td>                            
                             <button className='date-button'><i className="glyphicon glyphicon-calendar"></i> {finalDate} </button>
                            </td>
                        </tr>
                        <tr>Hours: 
                            <td><p className ="description-description-style">  
                        {
                            hours.map(hour=>
                            <button className='hour-button' type="submit" onClick = {() => this.goToChairSelect(hour.movieHourId)}><i className="glyphicon glyphicon-time"></i> {hour.stringHour} </button>
                            )
                        }</p></td> 
                        </tr>
                        <tr>Description: </tr>
                    </div>
                    <div className = "padding-total-description-description">
                         <tr> <p className ="description-bottom-style">  {this.state.movie.description}</p> </tr>
                    </div>
                    <button className='back-button' type="submit" onClick = {() => this.goToMovieList()}><i className="glyphicon glyphicon-circle-arrow-left"></i> Back </button>
                </th>
             </table>
             <img className ='description-image-padding' src={"data:image/jpeg;base64," + this.state.movie.image }/>
            </div>
        </div>
    }
}
