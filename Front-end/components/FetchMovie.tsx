﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';

interface FetchMovieDataState {
    movieList: MovieData[];
    loading: boolean
    oldCss: string[]
}


export class FetchMovie extends React.Component<RouteComponentProps<{}>, FetchMovieDataState> {
    constructor() {
        super();
        this.state = { movieList: [], loading: true, oldCss:["white","white","white","white","white","white","white","white"] };
        fetch('api/Movie/Index')
            .then(response => response.json() as Promise<MovieData[]>)
            .then(data => {
                this.setState({ movieList: data, loading: false });
            });
    }

    public render() {

        let contentNavBar = this.handleNavDaysBar();
        let contentMotto = this.motto();
        let contentScrollBar = this.scrollBar(this.state.movieList);
        let copyrightText = this.copyrightText();
        
        return <div>
             {contentMotto}
             {contentNavBar}
             {contentScrollBar}
             {copyrightText}
        </div>;
    }

    private printInformation(movieId: number)
    {
        
        this.props.history.push("/details/" + movieId);
    } 

    private copyrightText()
    {
            return <div style={{position:"relative"}}>
                <div className="copyRight-style">
                    All rights reserved to CinemaVision Romania 2018 ©
                </div>
            </div> 
    }
        
    componentDidMount() {
        window.addEventListener('table-scroll',  () => this.handleScroll);
    }
    
    componentWillUnmount() {
        window.removeEventListener('table-scroll', () => this.handleScroll);
    }
    

     handleScroll = (scrollNumber: number) => {

        let colorList : string[] = ["white","white","white","white","white","white","white","white"];
        colorList[scrollNumber] = "#2EBBDC";
        this.setState({ oldCss: colorList });
    }

    private imageShow(movieList:MovieData[], scrollNumber: number)
    {
        return <div id="table-scroll" onScroll = {() => this.handleScroll(scrollNumber)} >
        <table> 
            <tbody>
              <tr> <td>   
              {movieList.map(movie =>
              <div>
              <button className ='search-button'> {movie.name} </button>
              <button className ='image-padding' onClick = {() => this.printInformation(movie.movieId)}> <img src={"data:image/jpeg;base64," + movie.image} /> </button>
              </div>
              )}
              </td> </tr>
            </tbody>
        </table>
      </div>
    }


 
    private scrollBar(movieList: MovieData[])
    {
        return <div>
        <div id="table-wrapper">
        {this.imageShow(movieList,0)}
        {this.imageShow(movieList,1)}
        {this.imageShow(movieList,2)}
        {this.imageShow(movieList,3)}
        {this.imageShow(movieList,4)}
        {this.imageShow(movieList,5)}
        {this.imageShow(movieList,6)}
        </div>
        </div>
    }


    private motto()
    {
        return <div>
            <div className = 'motto-style'>
            Our <span className = 'color-style'> movies </span> depend on your <span className = 'color-style'> Vision! </span>
            </div>
         </div>
    }


    private handleNavDaysBar()
    {
        return <div>
            <div className = 'schedule-style'>
            Schedule
          </div>
            <nav className="navbar navbar-inverse">
            <div className="container-fluid">
                <p className="navbar-text" style={{'color':this.state.oldCss[0]}}>Monday</p>
                <p className="navbar-text" style={{'color':this.state.oldCss[1]}}>Tuesday</p>
                <p className="navbar-text" style={{'color':this.state.oldCss[2]}}>Wednesday</p>
                <p className="navbar-text" style={{'color':this.state.oldCss[3]}}>Thursday</p>
                <p className="navbar-text" style={{'color':this.state.oldCss[4]}}>Friday</p>
                <p className="navbar-text" style={{'color':this.state.oldCss[5]}}>Saturday</p>
                <p className="navbar-text" style={{'color':this.state.oldCss[6]}}>Sunday</p>
                <form className="navbar-form" role="search">
                <div className="input-group">
                    <div className="input-group-btn">
                        <button className='search-button' type="submit"><i className="glyphicon glyphicon-search"></i> Search </button>
                    </div>
                    <input type="text" className = 'input'/>
                </div>
                </form>
            </div>
            </nav>
       </div>
    }
}

export class MovieData {
    movieId: number = 0;
    name: string = "";
    description: string = "";
    rating: number = 0;
    category: string = "";
    type: string = "";
    distribution: string = "";
    director: string = "";
    language: string = "";
    time: number = 0;
    launchDate: Date = new Date();
    hours: HourData[] = [new HourData()];
    image: string = "";
 }

export class HourData{
    movieHourId: number = 0;
    stringHour: string = "";
    movieId? : number = 0;
}