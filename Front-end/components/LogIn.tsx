import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import './../css/login.css';

interface UserDatState {
    user: UserData,
    check: boolean,
    password: string,
    email: string
}


export class LogIn extends React.Component<RouteComponentProps<any>, UserDatState> {

    constructor() {  
        super(); 
        this.state = { user: new UserData, check: false , password: "", email: ""};
    }

    public render() {
        return <div>
            <div className = 'motto-style'>
                Our <span className = 'color-style'> movies </span> depend on your <span className = 'color-style'> Vision! </span>
            </div>
            {this.printLogIn()}
        </div>
        }

    private printLogIn()
    {
        return (
        <div className = "login-padding">
            <form className="login">
                <img src={require('./../images/navbarLogo.png')} className = "logIn-cinema" />
                <input 
                    type="text" 
                    className="login-input" 
                    required
                    onChange={event => this.setState({email: event.target.value})}
                />
                <input
                type="password" 
                className="login-input"
                required
                onChange={event => this.setState({password: event.target.value})}
                />
                <button  className="login-button" onClick = {(e) => this.checkAccount(e)}>
                Log In
                </button>
            </form>
            </div>
        )
    }
    private checkAccount = (event:any) => {
       event.preventDefault(); 
       const info = new FormData();
       info.append("Email", this.state.email);
       info.append("Password", this.state.password);

        fetch('api/User/', {
            method: 'POST',
            body: info, 
        }).then((response) => response.json())  
        .then((responseJson) => {
                    if(responseJson == true)
                    this.props.history.push ("/admin");
                    else{
                        alert("Email address or password are wrong!")
                    }
        })
    }  


}

export class UserData{
    userId: number = 0;
    email: string = "";
    password: string ="";
}