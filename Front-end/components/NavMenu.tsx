import * as React from 'react';
import { Link, NavLink } from 'react-router-dom';

export class NavMenu extends React.Component<{}, {}> {
    public render() {
        return <div className='main-nav'>
                <img src={require('./../images/Logo.png')}width="80%" height="150" className='logo-style' />
                <div className='navbar navbar-inverse'>
                <div className='navbar-header'>
                    <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                        <span className='sr-only'>Toggle navigation</span>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                    </button>
                </div>
                <div className='clearfix'></div>
                <div className='navbar-collapse collapse'>
                    <ul className='nav navbar-nav'>
                        <li>
                            <NavLink to={ '/' } exact activeClassName='active'>
                                <span className='glyphicon glyphicon-home'></span> Home
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to={ '/movies' } activeClassName='active'>
                                <span className='glyphicon glyphicon-film'></span> Movies
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to={ '/contact' } activeClassName='active'>
                                <span className='glyphicon glyphicon-info-sign'></span> Contact
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to={ '/logIn' } activeClassName='active'>
                                <span className='glyphicon glyphicon-user'></span> Admin
                            </NavLink>
                        </li>
                    </ul>
                    <img src={require('./../images/navbarLogo.png')}width="100%" height="100%" />
                </div>
            </div>
        </div>
    }
}
