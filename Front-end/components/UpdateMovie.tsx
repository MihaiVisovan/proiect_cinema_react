import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { MovieData, HourData} from './FetchMovie';


interface AddMovieDataState {
    title: string,
    loading: boolean,
    movieData: MovieData,
    file: '',
    imagine: ""
}

export class UpdateMovie extends React.Component<RouteComponentProps<any>, AddMovieDataState> {

    constructor() {
        super();
        this.state = { title: "", loading: true, movieData: new MovieData, file: '', imagine: ""};


        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    public render() {

        let contents = this.state.loading
        ? <p><em>Loading...</em></p>
        : this.renderCreateForm();
        
        const movieId = this.props.match.params.movieId;
        if (movieId > 0) {
            fetch('api/Movie/Details/' + movieId)
                .then(response => response.json() as Promise<MovieData>)
                .then(data => {
                    this.setState({ title: "Edit", loading: false, movieData: data });
                });
        }
        else if(this.state.loading == true)
        {
            this.setState({ title: "Create", loading: false, movieData: new MovieData });
        }
     
        return <div>
            <h2>{this.state.title}</h2>
            <h3>Movie</h3>
            <hr />
            {contents}
        </div>;
    }

    // This will handle the submit form event.
    private handleSave(event: any) {
        event.preventDefault();
        const data = new FormData(event.target);

        // PUT request for Edit movie.
        if (this.state.movieData.movieId) {
            fetch('api/Movie/Edit', {
                method: 'PUT',
                body: data,
            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/admin");
                })
        }

        // POST request for Add movie.
        else {
            fetch('api/Movie/Create', {
                method: 'POST',
                body: data,
            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/admin");
                })
        }
    }

    private handleCancel(e: any) {
        e.preventDefault();
        this.props.history.push("/admin");
    }

    private renderCreateForm() {
        return (
            <form onSubmit={this.handleSave} >
                <div className="form-group row" >
                    <input type="hidden" name="movieId" value={this.state.movieData.movieId} />
                </div>
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Name">Name</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="name" defaultValue={this.state.movieData.name} required />
                    </div>
                </div >
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Description">Description</label>
                    <div className="col-md-4">
                         <input className="form-control" type="text" name="Description" defaultValue={this.state.movieData.name} required />    
                    </div>
                </div >
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Rating" >Rating</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="Rating" defaultValue={this.state.movieData.rating.toString()} required />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Category" >Category</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="Category" defaultValue={this.state.movieData.category} required />
                    </div>
                </div><div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Type" >Type</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="Type" defaultValue={this.state.movieData.type} required />
                    </div>
                </div><div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Distribution" >Distribution</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="Distribution" defaultValue={this.state.movieData.distribution} required />
                    </div>
                </div><div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Director" >Director</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="Director" defaultValue={this.state.movieData.director} required />
                    </div>
                </div><div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Language" >Language</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="Language" defaultValue={this.state.movieData.language} required />
                    </div>
                </div><div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Time" >Time</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="Time" defaultValue={this.state.movieData.time.toString()} required />
                    </div>
                </div><div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="LaunchDate" >LaunchDate</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="LaunchDate" defaultValue={this.state.movieData.launchDate.toString()} required />
                    </div>
                </div>
                {/* TODO */}
                {/* <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="Available Hours" >Available Hours</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="Available Hours" defaultValue={this.state.movieData.hours[0].stringHour} required />
                    </div>
                </div> */}
                    {/* <label >
                        <input className="fileInput" type="file" name="imagine" required onChange={(e) => this.handleImageChange(e)}/>
                    </label> */}
                <div className="form-group">
                    <button type="submit" className="btn btn-default">Save</button>
                    <button className="btn" onClick={this.handleCancel}>Cancel</button>
                </div >
            </form >
        )
    }
}