import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchMovie } from './components/FetchMovie';
import { Contact } from './components/Contact';
import { Details } from './components/Details';
import { ChairSelect } from './components/ChairSelect';
import { LogIn } from './components/LogIn';
import { Admin } from './components/Admin';
import { UpdateMovie } from './components/UpdateMovie';



export const routes = <Layout>
    <Route exact path='/' component={ Home } />
    <Route path='/movies' component={FetchMovie} />
    <Route path='/contact' component={Contact} />
    <Route path='/details/:id' component={Details}/>
    <Route path='/chairSelect/:idMovie/hour/:idHour' component={ChairSelect}/>
    <Route path='/logIn' component={LogIn}/>
    <Route path='/admin' component={Admin}/>
    <Route path='/updateMovie/:movieId' component={UpdateMovie}/>
</Layout>;
